var map;

function initMap() {

  // Create a map object and specify the DOM element for display.
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 34.0899, lng: -117.4339},
    scrollwheel: true,
    mapTypeId: google.maps.MapTypeId.HYBRID,
    zoom: 14
  });

  var geocoder = new google.maps.Geocoder();

  $('#formRequestService').submit(function(e) {
    e.preventDefault();
    geocodeAddress(geocoder, map);

    expandMap(map);
    promptAddress();

  });

  /*
  document.getElementById('submit').addEventListener('click', function(e){
  	e.preventDefault();
  	geocodeAddress(geocoder, map);
  });
  */

}

function geocodeAddress(geocoder, resultsMap) {
  var address = document.getElementById('inputZipCode').value;
  geocoder.geocode({'address': address}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      resultsMap.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
        map: resultsMap,
        position: results[0].geometry.location
      });
    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

// User Interface / Animations
function expandMap (map) {
  $('#divMap').toggleClass('min');
  setTimeout(function() {
    google.maps.event.trigger(map, 'resize');
    $('#divMap .overlay').toggleClass('front')
  }, 450);
}

function promptAddress() {
  $('#divZipCode').slideToggle();
  $('#divAddress').slideToggle();
}